<?php

namespace App\Http\Middleware;

use Closure;

class LoggedInMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(session('userName') == null){
            return redirect('/');
        }
        return $next($request);
    }
}
