<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\TokenStore\TokenCache;
use App\TimeZones\TimeZones;
use Exception;
class CalendarController extends Controller
{
  private function getGraph(): Graph
  {
    // Get the access token from the cache
    $tokenCache = new TokenCache();
    $accessToken = $tokenCache->getAccessToken();
    // Create a Graph client
    $graph = new Graph();
    $graph->setAccessToken($accessToken);
    return $graph;
  }

  //view default calendar events
  public function calendar()
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    // Get user's timezone
    $timezone = TimeZones::getTzFromWindows($viewData['userTimeZone']);
    // Get start and end of week
    $startOfWeek = new \DateTimeImmutable('sunday -1 week', $timezone);
    $endOfWeek = new \DateTimeImmutable('sunday', $timezone);

    $queryParams = array(
      //ISO8601 format wont work with php version 7.0.33 thats why comment bellow code
      // 'startDateTime' => $startOfWeek->format(\DateTimeInterface::ISO8601),
      // 'endDateTime' => $endOfWeek->format(\DateTimeInterface::ISO8601),

      //use DateTime because it works with php version 7.0.33
      'startDateTime' => $startOfWeek->format(\DateTime::ISO8601),
      'endDateTime' => $endOfWeek->format(\DateTime::ISO8601),

      // Only request the properties used by the app
      '$select' => 'subject,organizer,start,end',
      // Sort them by start time
      '$orderby' => 'start/dateTime desc',
      // Limit results to 25
      '$top' => 30
    );

    // Append query parameters to the '/me/calendarView' url
    $getEventsUrl = '/me/calendarView?'.http_build_query($queryParams);
    $events = $graph->createRequest('GET', $getEventsUrl)
      // Add the user's timezone to the Prefer header
      ->addHeaders(array(
        'Prefer' => 'outlook.timezone="'.$viewData['userTimeZone'].'"'
      ))
      ->setReturnType(Model\Event::class)
      ->execute();
 
    $viewData =array('startDate'=>date("d/m/Y", strtotime($queryParams['startDateTime'])),'endDate'=>date("d/m/Y", strtotime($queryParams['endDateTime'])), 'events'=>$events);
    return view('calendar', $viewData);
  }  

  //new event form
  public function getNewEventForm()
  {
    return view('newevent');
  }

  //create default calendar event
  public function createNewEvent(Request $request)
  {
    $this->validate($request, [
      'eventSubject' => 'required|string',
      'eventAttendees' => 'required|string',
      'eventStart' => 'required|date',
      'eventEnd' => 'required|date',
      'eventBody' => 'string'
    ]);

    //validate if events trying to add for past dates
    if( (date("dmY", strtotime($request->eventStart)) < date('dmY')) ||( date("dmY", strtotime($request->eventEnd)) < date('dmY')) ){
        return back()->withInput()->withErrors(['errorMsg'=>'Event can not be added for past dates']);
    }

    //if both date are same & trying to add past time then show error
    if(date("dmY", strtotime($request->eventStart)) == date('dmY') && date("Hi", strtotime($request->eventStart)) < date('Hi')){
        return back()->withInput()->withErrors(['errorMsg'=>'Event can not be added for past time']);
    }
    //validate if start date is greater than end date
    if(date("dmY", strtotime($request->eventStart)) > date("dmY", strtotime($request->eventEnd))){
        return back()->withInput()->withErrors(['errorMsg'=>'The event start date must be smaller than end date.']);
    }
    //validate if start time is greater than end time
    if((date("dmY", strtotime($request->eventStart)) == date("dmY", strtotime($request->eventEnd))) && (date("Hi", strtotime($request->eventStart)) > date("Hi", strtotime($request->eventEnd)))){
        return back()->withInput()->withErrors(['errorMsg'=>'The event start time must be smaller than end time.']);
    }

    $viewData = $this->loadViewData();
    $graph = $this->getGraph();

    $attendeeAddresses = explode(',', $request->eventAttendees);
    $attendees = [];
    foreach($attendeeAddresses as $attendeeAddress)
    {
      array_push($attendees, [
        // Add the email address in the emailAddress property
        'emailAddress' => [
          'address' => $attendeeAddress
        ],
        'type' => 'required'
      ]);
    }

    $newEvent = [
      'subject' => $request->eventSubject,
      'attendees' => $attendees,
      'start' => [
        'dateTime' => $request->eventStart,
        'timeZone' => $viewData['userTimeZone']
      ],
      'end' => [
        'dateTime' => $request->eventEnd,
        'timeZone' => $viewData['userTimeZone']
      ],
      'body' => [
        'content' => $request->eventBody,
        'contentType' => 'text'
      ]
    ];

    //trigger exception in a "try" block (error.exception will occure if first date value is greater than last date value)
    try{
        
      $response = $graph->createRequest('POST', '/me/events')
      ->attachBody($newEvent)
      ->setReturnType(Model\Event::class)
      ->execute();
      return redirect('/calendar')->with('message', 'Event has been added.');
      throw new Exception(); //throgh exception if any occure
    }
    //catch exception
    catch(Exception $e) {
      // echo $e->getResponse()->getstatusCode();
      return redirect()->back()->with('errorMsg', 'fields must be filled with valid data.');
    }
  }

  //to edit event form
  public function editEvent($event_id)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $url = "/me/events/".$event_id;
    $event = $graph->createRequest('GET', $url)
      ->setReturnType(Model\Event::class)
      ->addHeaders(array(
        'Prefer' => 'outlook.timezone="'.$viewData['userTimeZone'].'"'
      ))
      ->execute();
    $viewData['event'] = $event;
    return view('editevent', $viewData);
  }

  //update event
  public function updateEvent(Request $request){
    $this->validate($request, [
      'eventId' => 'required|string',
      'eventSubject' => 'required|string',
      'eventAttendees' => 'required|string',
      'eventStart' => 'required|date',
      'eventEnd' => 'required|date',
      'eventBody' => 'string'
    ]);

    //validate if events trying to add for past dates
    if( (date("dmY", strtotime($request->eventStart)) < date('dmY')) ||( date("dmY", strtotime($request->eventEnd)) < date('dmY')) ){
        return back()->withInput()->withErrors(['errorMsg'=>'Event can not be added for past dates']);
    }

    //if both date are same & trying to add past time then show error
    if(date("dmY", strtotime($request->eventStart)) == date('dmY') && date("Hi", strtotime($request->eventStart)) < date('Hi')){
        return back()->withInput()->withErrors(['errorMsg'=>'Event can not be added for past time']);
    }

    //validate if start date is greater than end date
    if(date("dmY", strtotime($request->eventStart)) > date("dmY", strtotime($request->eventEnd))){
      return redirect()->back()->with('errorMsg', 'The event start date must be smaller than end date.');
    }

    //validate if start time is greater than end time
    if((date("dmY", strtotime($request->eventStart)) == date("dmY", strtotime($request->eventEnd))) && (date("Hi", strtotime($request->eventStart)) > date("Hi", strtotime($request->eventEnd)))){
      return redirect()->back()->with('errorMsg', 'The event start time must be smaller than end time.');
    }

    $viewData = $this->loadViewData();
    $graph = $this->getGraph();

    // Attendees from form are a semi-colon delimited list of
    $attendeeAddresses = explode(',', $request->eventAttendees);
    // The Attendee object in Graph is complex, so build the structure
    $attendees = [];
    foreach($attendeeAddresses as $attendeeAddress)
    {
      array_push($attendees, [
        // Add the email address in the emailAddress property
        'emailAddress' => [
          'address' => $attendeeAddress
        ],
        // Set the attendee type to required
        'type' => 'required'
      ]);
    }

    // Build the event
    $newEvent = [
      'subject' => $request->eventSubject,
      'attendees' => $attendees,
      'start' => [
        'dateTime' => $request->eventStart,
        'timeZone' => $viewData['userTimeZone']
      ],
      'end' => [
        'dateTime' => $request->eventEnd,
        'timeZone' => $viewData['userTimeZone']
      ],
      'body' => [
        'content' => $request->eventBody,
        'contentType' => 'text'
      ]
    ];

    //trigger exception in a "try" block (error.exception will occure if first date value is greater than last date value)
    try {
       $url = "/me/events/".$request->eventId;
        $eventList = $graph->createRequest('PATCH', $url)
        ->attachBody($newEvent)
        ->setReturnType(Model\Event::class)
        ->execute();
        return redirect('/calendar')->with('message', 'Event has been updated.');
        throw new Exception();
    }
    //catch exception
    catch(Exception $e) {
      // echo $e->getResponse()->getstatusCode();
      return redirect()->back()->with('errorMsg', 'fields must be filled with valid data.');
    }
  }

  //to remove event by event id
  public function removeEvent($event_id)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
   // /me/events/{id}
    $url = "/me/events/".$event_id;
    $eventList = $graph->createRequest('DELETE', $url)
      ->setReturnType(Model\Event::class)
      ->execute();
    return redirect()->back()->with('errorMsg', 'Event Removed');
  }

  //view/list of all calendars
  public function allCalendarView()
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $url = "/me/calendars";
    $calendarList = $graph->createRequest('GET', $url)
      ->setReturnType(Model\Calendar::class)
      ->execute();
    $viewData['calendars'] = $calendarList;
    return view('allcalendars', $viewData);
  }
  //create new calendar form
  public function createNewCalendarForm()
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $getEventsUrl = '/me/calendarGroups';
    $groups = $graph->createRequest('GET', $getEventsUrl)
      ->addHeaders(array(
        'Prefer' => 'outlook.timezone="'.$viewData['userTimeZone'].'"'
      ))
      ->setReturnType(Model\Group::class) //changed to get group list
      ->execute();

      $viewData['groups'] = $groups;
      return view('newcalendar', $viewData);
  }

  //create new calendar 
  public function createNewCalendar(Request $request){
    $this->validate($request, [
    'groupId' => 'required|string',
    'CalendarName' => 'required|string'
    ]);
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $calendar =[
      "name" => $request->CalendarName
    ];
    $url = "/me/calendarGroups/".$request->groupId."/calendars";
    $response = $graph->createRequest('POST', $url)
      ->attachBody($calendar)
      ->setReturnType(Model\Group::class)
      ->execute();
    return redirect('/calendar/allcalendars')->with('message', 'Calendar has been added.');
  }

  //to edit calendar form
  public function editCalendar($calendar_id)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    // /me/calendars/{id}
    $url = "/me/calendars/".$calendar_id;
    $calendar = $graph->createRequest('GET', $url)
      ->setReturnType(Model\Calendar::class)
      ->execute();
    $viewData['calendar'] = $calendar;
    return view('editcalendar', $viewData);
  }

  //to update calendar 
  public function updateCalendar(Request $request)
  {
    $this->validate($request, [
    // 'groupId' => 'required|string',
    'calendarName' => 'required|string',
    'calendarId' => 'required|string'
    ]);

    $viewData = $this->loadViewData();
    $graph = $this->getGraph();

    $calendar =[
      "name" => $request->calendarName
    ];

    $url = "/me/calendars/".$request->calendarId;
    $response = $graph->createRequest('PATCH', $url)
      ->attachBody($calendar)
      ->setReturnType(Model\Group::class)
      ->execute();
    return redirect('/calendar/allcalendars')->with('message', 'Calendar has been updated.');
    // return redirect('/calendar/allcalendars');
  }

  //to remove calendar by calendar id
  public function removeCalendar($calendar_id)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $url = "/me/calendars/".$calendar_id;
    $calendar = $graph->createRequest('DELETE', $url)
      ->setReturnType(Model\Calendar::class)
      ->execute();
    return redirect()->back()->with('message', 'Calendar Removed');
  }

  //form to create new event(calendar specific event)
  public function getNewCalendarSpecificEventForm()
  {
      $viewData = $this->loadViewData();
      $graph = $this->getGraph();
      $getEventsUrl = '/me/calendarGroups';
      $groups = $graph->createRequest('GET', $getEventsUrl)
      ->addHeaders(array(
        'Prefer' => 'outlook.timezone="'.$viewData['userTimeZone'].'"'
      ))
      ->setReturnType(Model\Group::class) //changed to get group list
      ->execute();

      $viewData['groups'] = $groups;
      return view('neweventcalendarspecific', $viewData);
  }

  //create new event(calendar specific event)
  public function createNewCalendarSpecificEvent(Request $request)
  {
    $this->validate($request, [
      'calendarId' => 'required|string',
      'eventSubject' => 'required|string',
      'eventAttendees' => 'required|string',
      'eventStart' => 'required|date',
      'eventEnd' => 'required|date',
      'eventBody' => 'string'
    ]);
    //validate if events trying to add for past dates
    if( (date("dmY", strtotime($request->eventStart)) < date('dmY')) ||( date("dmY", strtotime($request->eventEnd)) < date('dmY')) ){
        return back()->withInput()->withErrors(['errorMsg'=>'Event can not be added for past dates']);
    }
    //if both date are same & trying to add past time then show error
    if(date("dmY", strtotime($request->eventStart)) == date('dmY') && date("Hi", strtotime($request->eventStart)) < date('Hi')){
        return back()->withInput()->withErrors(['errorMsg'=>'Event can not be added for past time']);
    }
     //validate if start date is greater than end date
    if(date("dmY", strtotime($request->eventStart)) > date("dmY", strtotime($request->eventEnd))){
        return back()->withInput()->withErrors(['errorMsg'=>'The event start date must be smaller than end date.']);
    }
    //validate if start time is greater than end time
    if((date("dmY", strtotime($request->eventStart)) == date("dmY", strtotime($request->eventEnd))) && (date("Hi", strtotime($request->eventStart)) > date("Hi", strtotime($request->eventEnd)))){
        return back()->withInput()->withErrors(['errorMsg'=>'The event start time must be smaller than end time.']);
    }

    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    // Attendees from form are a semi-colon delimited list of
    $attendeeAddresses = explode(',', $request->eventAttendees);
    // The Attendee object in Graph is complex, so build the structure
    $attendees = [];
    foreach($attendeeAddresses as $attendeeAddress)
    {
      array_push($attendees, [
        // Add the email address in the emailAddress property
        'emailAddress' => [
          'address' => $attendeeAddress
        ],
        // Set the attendee type to required
        'type' => 'required'
      ]);
    }
    $newEvent = [
      'subject' => $request->eventSubject,
      'attendees' => $attendees,
      'start' => [
        'dateTime' => $request->eventStart,
        'timeZone' => $viewData['userTimeZone']
      ],
      'end' => [
        'dateTime' => $request->eventEnd,
        'timeZone' => $viewData['userTimeZone']
      ],
      'body' => [
        'content' => $request->eventBody,
        'contentType' => 'text'
      ]
    ];

    //  /me/calendars/{id}/events  -- to set event on specific calendar
    $url = "/me/calendars/".$request->calendarId."/events";
    $eventList = $graph->createRequest('POST', $url)
    ->attachBody($newEvent)
    ->setReturnType(Model\Event::class)
    ->execute();
    return redirect("/calendar/events/".$request->calendarId)->with('message', 'Event Added');
  }


  //group specific calendar
  public function calendarView($group_id)
  {

    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    //api all group list
    //me/calendargroups
    $url = "/me/calendargroups/".$group_id."/calendars";
    $calendarList = $graph->createRequest('GET', $url)
      ->setReturnType(Model\Calendar::class)
      ->execute();
    $viewData['calendars'] = $calendarList;
    return view('calendars', $viewData);
  }


  public function eventView($calendar_id)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    // Get user's timezone
    $timezone = TimeZones::getTzFromWindows($viewData['userTimeZone']);

    // Get start and end of week
    $startOfWeek = new \DateTimeImmutable('sunday -1 week', $timezone);
    $endOfWeek = new \DateTimeImmutable('sunday', $timezone);

    $queryParams = array(
      //use DateTime because it works with php version 7.0.33
      'startDateTime' => $startOfWeek->format(\DateTime::ISO8601),
      'endDateTime' => $endOfWeek->format(\DateTime::ISO8601),

      //Sort them by start time
      '$orderby' => 'start/dateTime desc',
      //Limit results to 25
      '$top' => 40
    );
   
    // Append query parameters to the '/me/calendarView' url
    $getEventsUrl = '/me/calendars/'.$calendar_id.'/calendarView?'.http_build_query($queryParams);
    // dd($getEventsUrl);
    $eventList = $graph->createRequest('GET', $getEventsUrl)
      // Add the user's timezone to the Prefer header
      ->addHeaders(array(
        'Prefer' => 'outlook.timezone="'.$viewData['userTimeZone'].'"'
      ))
      ->setReturnType(Model\Event::class)
      ->execute();
    $viewData =array('startDate'=>date("d/m/Y", strtotime($queryParams['startDateTime'])),'endDate'=>date("d/m/Y", strtotime($queryParams['endDateTime'])),'calendar_id'=>$calendar_id, 'events'=>$eventList);
    return view('events', $viewData);
  }

  //all group view/list
  public function groupView()
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $groupList = $graph->createRequest('GET', '/me/calendarGroups')
      ->setReturnType(Model\Group::class)
      ->execute();
    $viewData['groups'] = $groupList;
    return view('groups', $viewData);
  }

  //create group form
  public function createNewGroupForm()
  {
    return view('newgroup');
  }

  //create new group
  public function createNewGroup(Request $request){
    $this->validate($request, [
      'groupName' => 'required|string'
    ]);
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $groupName =[
      "name" => $request->groupName
    ];
    $response = $graph->createRequest('POST', '/me/calendarGroups')
      ->attachBody($groupName)
      ->setReturnType(Model\Event::class)
      ->execute();
    return redirect('/calendar/groups')->with('message', 'Group has been added.');
  }

  //to show edit group form/view
  public function editGroup($group_id)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $url = "/me/calendarGroups/".$group_id;
    $group = $graph->createRequest('GET', $url)
      ->setReturnType(Model\Group::class)
      ->execute();
    $viewData['group'] = $group;
    return view('editgroup', $viewData);
  }  

  //to update Group view
  public function updateGroup(Request $request)
  {
    $this->validate($request, [
    // 'groupId' => 'required|string',
    'groupName' => 'required|string',
    'groupId' => 'required|string'
    ]);

    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $calendar =[
      "name" => $request->groupName
    ];
    $url = "/me/calendarGroups/".$request->groupId;
    $response = $graph->createRequest('PATCH', $url)
      ->attachBody($calendar)
      ->setReturnType(Model\Group::class)
      ->execute();
    return redirect('/calendar/groups')->with('message', 'Group has been updated.');
  } 

  //to remove group by group by id
  public function removeGroup($group_id)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph(); 

    //check if group has child calendars (if it has then can't delete this group)
    $url = "/me/calendargroups/".$group_id."/calendars";
    $calendarList = $graph->createRequest('GET', $url)
      ->setReturnType(Model\Group::class)
      ->execute();
    $viewData['calendars'] = $calendarList;
    foreach($viewData['calendars'] as $calendar){
      return redirect()->back()->with('message', 'Group can not delete, because this group contain calendars.');
    }
    //end check if group has child calendars 

    $url = "/me/calendarGroups/".$group_id;
    $calendar = $graph->createRequest('DELETE', $url)
      ->setReturnType(Model\Group::class)
      ->execute();
    return redirect()->back()->with('message', 'Group Removed');
  }

  //get list of calendars using group id (in select/dropdown format) - used for ajax call
  public function getCalendar($group_id)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    //api get calender using calendar group id
    //me/calendargroups/{calendar_group_id}/calendars
    $url = "/me/calendargroups/".$group_id."/calendars";
    $calendarList = $graph->createRequest('GET', $url)
      ->setReturnType(Model\Group::class)
      ->execute();
    $viewData['calendars'] = $calendarList;

    echo"<select class='form-control' name='calendarId' id='calendarId' required=''>
          <option value=''>Select Calendar</option>";
          if(isset($viewData['calendars'])){
            foreach($viewData['calendars'] as $calendar){
              echo"<option value='".$calendar->getProperties()['id']."'>".$calendar->getProperties()['name']."</option>";
            }
          }
    echo"</select>";
  }

  //date range filter for default calendar - ajax call(used in calendar.blade.php)
  public function dateRangeEvent(Request $request)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    // Get user's timezone
    $timezone = TimeZones::getTzFromWindows($viewData['userTimeZone']);
    $queryParams = array(
      'startDateTime' => $request->eventStart."T00:00:00+0530",
      'endDateTime' => $request->eventEnd."T23:59:00+0530",
      // Only request the properties used by the app
      '$select' => 'subject,organizer,start,end',
      // Sort them by start time
      '$orderby' => 'start/dateTime desc',
      // Limit results to 25
     '$top' => $request->entries

    );
    $getEventsUrl = '/me/calendarView?'.http_build_query($queryParams);
    $eventList = $graph->createRequest('GET', $getEventsUrl)
      ->addHeaders(array(
        'Prefer' => 'outlook.timezone="'.$viewData['userTimeZone'].'"'
      ))
      ->setReturnType(Model\Event::class)
      ->execute();

      if(isset($eventList)){
      $i = 1;
      foreach($eventList as $event){
        echo"<tr>
            <td>".$i."</td>
            <td>".$event->getOrganizer()->getEmailAddress()->getName()."</td>
            <td>".$event->getSubject()."</td>
            <td>".\Carbon\Carbon::parse($event->getStart()->getDateTime())->format('n/j/y g:i A')."</td>
            <td>".\Carbon\Carbon::parse($event->getEnd()->getDateTime())->format('n/j/y g:i A')."</td>
            <td><a class='btn btn-primary btn-sm mb-3' href='/calendar/editevent/".$event->getProperties()['id']."'>Edit</a></td>
            <td><a class='btn btn-danger btn-sm mb-3' href='/calendar/removeevent/".$event->getProperties()['id']."'>Remove</a></td>
          </tr>";
          $i++;
        }
    }
  }

  //date range filter for default calendar - ajax call(used in events.blade.php)
  public function dateRangeCalEvent(Request $request)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    // Get user's timezone
    $timezone = TimeZones::getTzFromWindows($viewData['userTimeZone']);

    $queryParams = array(
      'startDateTime' => $request->eventStart."T00:00:00+0530",
      'endDateTime' => $request->eventEnd."T23:59:00+0530",
        // '$select' => 'subject,organizer,start,end,attendees',
        // Sort them by start time
      '$orderby' => 'start/dateTime desc',
        // Limit results to 
      '$top' => $request->entries
    );
    $getEventsUrl = '/me/calendars/'.$request->calendar_id.'/calendarView?'.http_build_query($queryParams);
    $eventList = $graph->createRequest('GET', $getEventsUrl)
      // Add the user's timezone to the Prefer header
      ->addHeaders(array(
        'Prefer' => 'outlook.timezone="'.$viewData['userTimeZone'].'"'
      ))
      ->setReturnType(Model\Event::class)
      ->execute();
    if(isset($eventList)){
      $i = 1;
      foreach($eventList as $event){
        echo"<tr>
            <td>".$i."</td>
            <td>".$event->getOrganizer()->getEmailAddress()->getName()."</td>
            <td>".$event->getSubject()."</td>
            <td>".\Carbon\Carbon::parse($event->getStart()->getDateTime())->format('n/j/y g:i A')."</td>
            <td>".\Carbon\Carbon::parse($event->getEnd()->getDateTime())->format('n/j/y g:i A')."</td>
            <td>";

              for($j=0; $j < count($event->getAttendees()); $j++){
               echo"<a href='#'' data-toggle='tooltip' title='".$event->getAttendees()[$j]['emailAddress']['name']."'>".str_limit($event->getAttendees()[$j]['emailAddress']['name'], $limit = 10, $end = '..').",
               </a>";
              }
            echo"</td>
            <td><a class='btn btn-primary btn-sm mb-3' href='/calendar/editevent/".$event->getProperties()['id']."'>Edit</a></td>
            <td><a class='btn btn-danger btn-sm mb-3' href='/calendar/removeevent/".$event->getProperties()['id']."'>Remove</a></td>
          </tr>";
          $i++;
        }
    }
  }

}