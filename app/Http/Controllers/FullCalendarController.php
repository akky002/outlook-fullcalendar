<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Http\Requests;
use Microsoft\Graph\Graph;
use Microsoft\Graph\Model;
use App\TokenStore\TokenCache;
use App\TimeZones\TimeZones;

class FullCalendarController extends Controller
{
  private function getGraph(): Graph
  {
    // Get the access token from the cache
    $tokenCache = new TokenCache();
    $accessToken = $tokenCache->getAccessToken();
    // Create a Graph client
    $graph = new Graph();
    $graph->setAccessToken($accessToken);
    return $graph;
  }

  public function calendar()
  {
    return view('fullcalendar/calendar');
  }

  public function getDefaultEvents(Request $request)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    // Get user's timezone
    $timezone = TimeZones::getTzFromWindows($viewData['userTimeZone']);
    // Get start and end of week
    $startOfWeek = new \DateTimeImmutable('sunday -1 week', $timezone);
    $endOfWeek = new \DateTimeImmutable('sunday', $timezone);

    $queryParams = array(
      'startDateTime' => $request->start,
      'endDateTime' => $request->end,
      // Only request the properties used by the app
      '$select' => 'subject,organizer,start,end',
      // Sort them by start time
      '$orderby' => 'start/dateTime',
      // Limit results to 500
      '$top' => 500
    );

    // Append query parameters to the '/me/calendarView' url
    $getEventsUrl = '/me/calendarView?'.http_build_query($queryParams);
    $events = $graph->createRequest('GET', $getEventsUrl)
      // Add the user's timezone to the Prefer header
      ->addHeaders(array(
        'Prefer' => 'outlook.timezone="'.$viewData['userTimeZone'].'"'
      ))
      ->setReturnType(Model\Event::class)
      ->execute();

    $eventsArr = [];
    foreach ($events as $event) {
    	array_push($eventsArr, array('id' => $event->getId(),'title' => $event->getSubject(), 'start' =>$event->getStart()->getDateTime(), 'end' =>$event->getEnd()->getDateTime()));
    }
	return json_encode($eventsArr);
  }

   //create default calendar event
  public function createNewEvent(Request $request)
  {
    $this->validate($request, [
      'eventSubject' => 'required|string',
      // 'eventAttendees' => 'required|string',
      'eventStart' => 'required|date',
      'eventEnd' => 'required|date',
      // 'eventBody' => 'string'
    ]);
    //validate if events trying to add for past dates
    if( (date("dmY", strtotime($request->eventStart)) < date('dmY')) ||( date("dmY", strtotime($request->eventEnd)) < date('dmY')) ){
        // return back()->withInput()->withErrors(['errorMsg'=>'Event can not be added for past dates']);
        $res = array('status'=>'400','message' => 'Event can not be added for past dates.');
		return json_encode($res);
    }

    //if both date are same & trying to add past time then show error
    if(date("dmY", strtotime($request->eventStart)) == date('dmY') && date("Hi", strtotime($request->eventStart)) < date('Hi')){
        // return back()->withInput()->withErrors(['errorMsg'=>'Event can not be added for past time']);
        $res = array('status'=>'400','message' => 'Event can not be added for past time.');
		return json_encode($res);
    }
    //validate if start date is greater than end date
    if(date("dmY", strtotime($request->eventStart)) > date("dmY", strtotime($request->eventEnd))){
        // return back()->withInput()->withErrors(['errorMsg'=>'The event start date must be smaller than end date.']);
        $res = array('status'=>'400','message' => 'The event start date must be smaller than end date.');
		return json_encode($res);
    }
    //validate if start time is greater than end time
    if((date("dmY", strtotime($request->eventStart)) == date("dmY", strtotime($request->eventEnd))) && (date("Hi", strtotime($request->eventStart)) > date("Hi", strtotime($request->eventEnd)))){
        // return back()->withInput()->withErrors(['errorMsg'=>'The event start time must be smaller than end time.']);
        $res = array('status'=>'400','message' => 'The event start time must be smaller than end time');
		return json_encode($res);
    }

    $viewData = $this->loadViewData();
    $graph = $this->getGraph();

    $attendeeAddresses = explode(',', $request->eventAttendees);
    $attendees = [];
    foreach($attendeeAddresses as $attendeeAddress)
    {
      array_push($attendees, [
        // Add the email address in the emailAddress property
        'emailAddress' => [
          'address' => $attendeeAddress
        ],
        'type' => 'required'
      ]);
    }

    $newEvent = [
      'subject' => $request->eventSubject,
      'attendees' => $attendees,
      'start' => [
        'dateTime' => $request->eventStart,
        'timeZone' => $viewData['userTimeZone']
      ],
      'end' => [
        'dateTime' => $request->eventEnd,
        'timeZone' => $viewData['userTimeZone']
      ]
    ];

    // dd($newEvent);

  	$response = $graph->createRequest('POST', '/me/events')
  	->attachBody($newEvent)
  	->setReturnType(Model\Event::class)
  	->addHeaders(array(
    	'Prefer' => 'outlook.timezone="'.$viewData['userTimeZone'].'"'
  	))
  	->execute();
    $res = array('status'=>'200','message' => 'Event has been added.');
	return json_encode($res);
  }

  //to remove event by event id
  public function removeEvent($event_id)
  {
    $viewData = $this->loadViewData();
    $graph = $this->getGraph();
    $url = "/me/events/".$event_id;
    $eventList = $graph->createRequest('DELETE', $url)
      ->setReturnType(Model\Event::class)
      ->execute();
    $res = array('status'=>'200','message' => 'Event has been removed.');
	return json_encode($res);
  }
}

