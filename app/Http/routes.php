<?php
use Illuminate\Support\Facades\Route;

Route::get('/', 'HomeController@welcome');
Route::get('/signin', 'AuthController@signin');
Route::get('/callback', 'AuthController@callback');
Route::get('/signout', 'AuthController@signout');

//allow only if user is loggedIn/ authorised
Route::group(['middleware' => ['loggedin']], function () {
	Route::get('/calendar', 'CalendarController@calendar');
	Route::get('/calendar/new', 'CalendarController@getNewEventForm');
	Route::post('/calendar/new', 'CalendarController@createNewEvent');

	Route::get('/calendar/groups', 'CalendarController@groupView');
	Route::get('/calendar/newgroup', 'CalendarController@createNewGroupForm');
	Route::post('/calendar/newgroup', 'CalendarController@createNewGroup');
	Route::get('/calendar/removegroup/{group_id}', 'CalendarController@removeGroup');
	Route::get('/calendar/editgroup/{group_id}', 'CalendarController@editGroup');
	Route::post('/calendar/updategroup', 'CalendarController@updateGroup');

	Route::get('/calendar/allcalendars', 'CalendarController@allCalendarView');
	Route::get('/calendar/calendars/{group_id}', 'CalendarController@calendarView');
	Route::get('/calendar/newcalendar', 'CalendarController@createNewCalendarForm');
	Route::post('/calendar/newcalendar', 'CalendarController@createNewCalendar');
	Route::get('/calendar/removecalendar/{calendar_id}', 'CalendarController@removeCalendar');
	Route::get('/calendar/editcalendar/{calendar_id}', 'CalendarController@editCalendar');
	Route::post('/calendar/updatecalendar', 'CalendarController@updateCalendar');

	Route::get('/calendar/events/{calendar_id}', 'CalendarController@eventView');
	Route::get('/calendar/newcalendarevent', 'CalendarController@getNewCalendarSpecificEventForm');
	Route::post('/calendar/newcalendarevent', 'CalendarController@createNewCalendarSpecificEvent');
	Route::get('/calendar/removeevent/{event_id}', 'CalendarController@removeEvent');
	Route::get('/calendar/editevent/{event_id}', 'CalendarController@editEvent');
	Route::post('/calendar/updateevent', 'CalendarController@updateEvent');

	Route::post('/calendar/daterangeevent', 'CalendarController@dateRangeEvent');
	Route::post('/calendar/events/daterangecalevent', 'CalendarController@dateRangeCalEvent');
	Route::get('/calendar/getcalendar/{group_id}', 'CalendarController@getCalendar');

	//fullcalendar routes
	Route::get('/fullcalendar/calendar', 'FullCalendarController@calendar');
	Route::post('/fullcalendar/getdefaultevents', 'FullCalendarController@getDefaultEvents');
	Route::post('/fullcalendar/createevent', 'FullCalendarController@createNewEvent');
	Route::get('/fullcalendar/removeevent/{event_id}', 'FullCalendarController@removeEvent');

});

























// Application (client) ID :   f2f80c1d-9843-43bb-bbf6-db62c7411dac   
//Directory (tenant) ID : 47eb5c5d-b704-45ca-8a61-80d6e9816cbd    
//client secret value(Forever)  :  AJOl~Fi~.Qet_zdBF9eB28_n2EIS9Qas1-