@extends('layout')
@section('content')
<h3>Edit Calendar</h3>
<form method="POST" action="{{ action('CalendarController@updateCalendar') }}">
  {{ csrf_field() }}
	<!-- <div class="form-group">
	  <label for="sel1">Select Group:</label>
	  <select class="form-control" name="groupId" required="">
	    <option value="">Select Group</option>
	    @if(isset($calendar))
	      @foreach($calendar as $group)
	        <option value="{{ $group->getProperties()['id'] }}">{{ $group->getProperties()['name'] }}</option>
	      @endforeach
	    @endif
	  </select>
	</div> -->
	<div class="form-group">
		<label>Calendar name:</label>
		<input type="text" class="form-control" name="calendarName"  value="{{$calendar->getProperties()['name']}}"required="" />
		<input type="hidden" class="form-control" name="calendarId"  value="{{$calendar->getProperties()['id']}}"required="" />
	</div>
	<input type="submit" class="btn btn-primary mr-2" value="Update" />
	<a class="btn btn-secondary" href={{ action('CalendarController@allCalendarView') }}>Cancel</a>
</form>
@endsection