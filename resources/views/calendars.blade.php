@extends('layout')
@section('content')
<h2>Group Specific Calendars</h2>

<a class="btn btn-primary btn-sm mb-3" href={{action('CalendarController@createNewCalendarForm')}}>New Calendar</a>

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif

<table class="table">
  <thead>
    <tr>
      <th scope="col">Sr.No.</th>
      <th scope="col">Group</th>
      <th scope="col">Remove</th>
    </tr>
  </thead>
  <tbody>
    @if(isset($calendars))
    	@php $i = 1 @endphp
	    @foreach($calendars as $calendar)
	    	<tr>
	    		<td>{{ $i}}</td>
          		<td><a class="btn btn-light btn-sm mb-3" href="{{ url('/calendar/events/'.$calendar->getProperties()['id']) }}">{{ $calendar->getProperties()['name'] }}</a></td>
              @if($calendar->getProperties()['name'] != 'Calendar')  
                <td><a class="btn btn-primary btn-sm mb-3" href="{{ url('/calendar/editcalendar/'.$calendar->getProperties()['id']) }}">Edit</a></td>
                <td><a class="btn btn-danger btn-sm mb-3" href="{{ url('/calendar/removecalendar/'.$calendar->getProperties()['id']) }}">Remove</a></td>
              @else
                <td data-toggle="tooltip" title="can't edit default calendar">---</td>
                <td data-toggle="tooltip" title="can't delete default calendar">---</td>
              @endif
        	</tr>
        	@php $i++; @endphp
        @endforeach
	@endif
  </tbody>
</table>
@endsection