@extends('layout')
@section('content')
<h3>All Groups</h3>
<a class="btn btn-primary btn-sm mb-3" href={{action('CalendarController@createNewGroupForm')}}>Create New Group</a>

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
<table class="table">
  <thead>
    <tr>
      <th scope="col">Sr.No.</th>
      <th scope="col">Group</th>
      <th scope="col">Edit</th>
      <th scope="col">Remove</th>
    </tr>
  </thead>
  <tbody>
    @if(isset($groups))
    	@php $i = 1 @endphp
	    @foreach($groups as $group)
	    	<tr>
	    		<td>{{ $i }}</td>
              <td><a class="btn btn-light btn-sm mb-3" href="{{ url('/calendar/calendars/'.$group->getProperties()['id']) }}">{{ $group->getProperties()['name'] }}</a></td>
              <td><a class="btn btn-primary btn-sm mb-3" href="{{ url('/calendar/editgroup/'.$group->getProperties()['id']) }}">Edit</a></td>
              @if($group->getProperties()['name'] != 'Other Calendars')  
                <td><a class="btn btn-danger btn-sm mb-3" href="{{ url('/calendar/removegroup/'.$group->getProperties()['id']) }}">Remove</a></td>
              @else
                <td data-toggle="tooltip" title="can't delete Other Calendars group">---</td>
              @endif 
        	</tr>
        	@php $i++; @endphp
        @endforeach
	@endif
  </tbody>
</table>
@endsection