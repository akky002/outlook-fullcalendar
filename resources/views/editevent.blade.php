@extends('layout')
@section('content')
<h3>Edit Calendar event</h3>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
@if(session()->has('errorMsg'))
    <div class="alert alert-danger">
        {{ session()->get('errorMsg') }}
    </div>
@endif
<form method="POST" action="{{ action('CalendarController@updateEvent') }}">
  {{ csrf_field() }}
  <div class="form-group">
    <label>Subject</label>
    <input type="text" class="form-control" name="eventSubject" value="{{$event->getSubject()}}" />
  </div>
  <div class="form-group">
    <label>Attendees (emails seperated by commas)</label>
    <textarea type="text" class="form-control" name="eventAttendees" rows="2">@for($j=0; $j < count($event->getAttendees()); $j++){{ $event->getAttendees()[$j]['emailAddress']['address'] }},@endfor</textarea>
  </div>
  <div class="form-row">
    <div class="col">
      <div class="form-group">
        <label>Start</label>
        <input type="datetime-local" class="form-control" name="eventStart" id="eventStart" value="{{substr($event->getStart()->getDateTime(), 0, -11)}}"/>
      </div>
    </div>
    <div class="col">
      <div class="form-group">
        <label>End</label>
        <input type="datetime-local" class="form-control" name="eventEnd" value="{{substr($event->getEnd()->getDateTime(), 0, -11)}}" />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Body</label>
    <textarea type="text" class="form-control" name="eventBody" rows="3">{{ $event->getBodyPreview() }}</textarea>
  </div>
  <input type="hidden" class="form-control" name="eventId"  value="{{$event->getProperties()['id']}}"required="" />

  <input type="submit" class="btn btn-primary mr-2" value="Update" />
  <a class="btn btn-secondary" href={{ action('CalendarController@calendar') }}>Cancel</a>
</form>
@endsection