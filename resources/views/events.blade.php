@extends('layout')
@section('content')
<h3>Calendar Specific Events</h3>
<a class="btn btn-primary btn-sm mb-3" href={{action('CalendarController@getNewEventForm')}}>Create New Default Calendar Event</a>

<a class="btn btn-primary btn-sm mb-3" href={{action('CalendarController@getNewCalendarSpecificEventForm')}}>Create New Calendar Specific event</a>
<hr/>

<!-- Start date range filter -->
<div class="row">
  <div class="col-sm-3">
      <div class="form-group">
        <label for="eventStart">Start Date:</label>
        <input type="date" class="form-control" name="eventStart" id="eventStart" value="@if(isset($eventStart) && isset($eventEnd)){{$eventStart}}@endif" required="">
      </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">
      <label for="eventEnd">End Date:</label>
      <input type="date" class="form-control" name="eventEnd" id="eventEnd"  value="@if(isset($eventStart) && isset($eventEnd)){{$eventEnd}}@endif" required="">
      <input type="hidden" class="form-control" name="calendar_id" id="calendarId" value="{{ request()->route('calendar_id')}}" required="">
    </div>
  </div>
  <div class="col-sm-3">
    <div class="form-group">
      <label for="eventEnd">Show Entries:</label>
      <select name="entries" id="entries" class="form-control">
        <option value="10">10</option>
        <option value="25">25</option>
        <option value="50">50</option>
        <option value="100">100</option>
      </select>
    </div>
  </div>
  <div class="col-sm-3" style="padding-top: 30px;">
      <input type="submit" class="btn btn-primary mr-2" value="Submit" onclick="dateRangeCalEvent()" />
      <a class="btn btn-secondary" href="{{ url()->previous() }}">Cancel</a>
  </div>
</div>
<p id="eventStartErr" class="alert alert-danger" style="display: none">Start Date should not empty.</p>
<p id="eventEndErr" class="alert alert-danger" style="display: none">End Date should not empty.</p>
<p id="dateRangeErr" class="alert alert-danger" style="display: none">Event start date must be smaller than end date.</p><hr/>
<h5 class="text-center" id="date_range"><b>Date Range : {{$startDate}} - {{$endDate}}</b></h5>
<!-- End date range filter -->

@if(session()->has('message'))
    <div class="alert alert-success">
        {{ session()->get('message') }}
    </div>
@endif
@if(session()->has('errorMsg'))
  <div class="alert alert-danger">
      {{ session()->get('errorMsg') }}
  </div>
@endif

<table class="table">
  <thead>
    <tr>
      <th scope="col">Sr.No.</th>
      <th scope="col">Organizer</th>
      <th scope="col">Subject</th>
      <th scope="col">Start</th>
      <th scope="col">End</th>
      <th scope="col">Attendees</th>
      <th scope="col">Edit</th>
      <th scope="col">Remove</th>
    </tr>
  </thead>
  <tbody id="eventsBody">

    @if(isset($events))
    	@php $i = 1 @endphp
	    @foreach($events as $event)
	    	<tr>
         	  <td>{{ $i}}</td>
	          <td>{{ $event->getOrganizer()->getEmailAddress()->getName() }}</td>
	          <td>{{ $event->getSubject() }}</td>
	          <td>{{ \Carbon\Carbon::parse($event->getStart()->getDateTime())->format('n/j/y g:i A') }}</td>
	          <td>{{ \Carbon\Carbon::parse($event->getEnd()->getDateTime())->format('n/j/y g:i A') }}</td>
            <td>
              @for($j=0; $j < count($event->getAttendees()); $j++)
               <a href="#" data-toggle="tooltip" title="{{$event->getAttendees()[$j]['emailAddress']['name']}}"> 
                {{ str_limit($event->getAttendees()[$j]['emailAddress']['name'], $limit = 10, $end = '..') }},
               </a>
              @endfor
            </td>
            <td><a class="btn btn-primary btn-sm mb-3" href="{{ url('/calendar/editevent/'.$event->getProperties()['id']) }}">Edit</a></td>
	          <td><a class="btn btn-danger btn-sm mb-3" href="{{ url('/calendar/removeevent/'.$event->getProperties()['id']) }}">Remove</a></td>
        	</tr>
        	@php $i++; @endphp
        @endforeach
	@endif
  </tbody>
</table>

<script type="text/javascript">
  function dateRangeCalEvent() {
    var token = "{{ csrf_token() }}";
    var calendarId = document.getElementById("calendarId").value;
    var eventStart = document.getElementById("eventStart").value;
    var eventEnd = document.getElementById("eventEnd").value;
    var entries = document.getElementById("entries").value;
    var eventStartFlag = false;
    var eventEndFlag = false;
    var dateRangeFlag = false;

    if(eventStart == ''){
        document.getElementById("eventStartErr").style.display = "block";
        eventStartFlag = true;
    }else{
        document.getElementById("eventStartErr").style.display = "none";
    }
    if(eventEnd == ''){
        document.getElementById("eventEndErr").style.display = "block";
        eventEndFlag = true;
    }else{
        document.getElementById("eventEndErr").style.display = "none";
    }
    //validate if start date is greater than end date
    if(moment(eventStart).format('DDMMYYYY') > moment(eventEnd).format('DDMMYYYY')){
        document.getElementById("dateRangeErr").style.display = "block";
        dateRangeFlag = true;
    }else{
        document.getElementById("dateRangeErr").style.display = "none";
    }

    if(eventStartFlag==false && eventEndFlag == false && dateRangeFlag == false){
      var xmlhttp = new XMLHttpRequest();
      xmlhttp.open("POST", "daterangecalevent", true);
      xmlhttp.setRequestHeader("Content-Type", "application/json");
      xmlhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          document.getElementById("eventsBody").innerHTML = this.responseText;
          document.getElementById("date_range").innerHTML = "<b>Date Range :"+ moment(eventStart).format('DD/MM/YYYY')+" - "+moment(eventEnd).format('DD/MM/YYYY')+"</b>";
        }
      }
      var data = {'_token':token,'calendar_id':calendarId,'eventStart': eventStart,'eventEnd':eventEnd,'entries':entries};
      xmlhttp.send(JSON.stringify(data));
    }
  }
</script>

@endsection