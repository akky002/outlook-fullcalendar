@extends('layout')
@section('content')
<h3>New calendar specific event</h3>
@if (count($errors) > 0)
  <div class="alert alert-danger">
    <ul>
        @foreach ($errors->all() as $error)
            <li>{{ $error }}</li>
        @endforeach
    </ul>
  </div>
@endif
@if(session()->has('errorMsg'))
    <div class="alert alert-danger">
        {{ session()->get('errorMsg') }}
    </div>
@endif
<form method="POST">
  {{ csrf_field() }}
  <div class="form-group">
	  <label for="sel1">Select Group</label>
	  <select class="form-control" name="groupId" id="groupId" value="{{ old('groupId') }}" onchange="sendGroupId()" required="">
	    <option value="">Select Group</option>
	    @if(isset($groups))
	      @foreach($groups as $group)
	        <option value="{{ $group->getProperties()['id'] }}">{{ $group->getProperties()['name'] }}</option>
	      @endforeach
	    @endif
	  </select>
  </div>
	<div class="form-group">
		<label for="sel1">Select Calendar</label>
	  <select class="form-control" name='calendarId' id='calendarId' value="{{ old('calendarId') }}"  required="">
		  <option value="">Select Calendar</option>
		</select>
	</div>
  <div class="form-group">
    <label>Subject</label>
    <input type="text" class="form-control" name="eventSubject"  value="{{ old('eventSubject') }}" />
  </div>
  <div class="form-group">
    <label>Attendees</label>
    <input type="text" class="form-control" name="eventAttendees"  value="{{ old('eventAttendees') }}" />
  </div>
  <div class="form-row">
    <div class="col">
      <div class="form-group">
        <label>Start</label>
        <input type="datetime-local" class="form-control" name="eventStart" id="eventStart"  value="{{ old('eventStart') }}"/>
      </div>
    </div>
    <div class="col">
      <div class="form-group">
        <label>End</label>
        <input type="datetime-local" class="form-control" name="eventEnd"  value="{{ old('eventEnd') }}" />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Body</label>
    <textarea type="text" class="form-control" name="eventBody" rows="3">{{ old('eventBody') }}</textarea>
  </div>
  <input type="submit" class="btn btn-primary mr-2" value="Create" />
  <a class="btn btn-secondary" href={{ action('CalendarController@calendar') }}>Cancel</a>
</form>
<script type="text/javascript">
	function sendGroupId() {
	  var groupId = document.getElementById("groupId").value;
	  var xmlhttp = new XMLHttpRequest();
	    xmlhttp.onreadystatechange = function() {
	      if (this.readyState == 4 && this.status == 200) {
	        document.getElementById("calendarId").innerHTML = this.responseText;
	      }
	    }
	    xmlhttp.open("GET", "getcalendar/"+groupId, true);
	    xmlhttp.send();
	}
</script>
@endsection

