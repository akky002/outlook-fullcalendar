@extends('layout')
@section('content')
<h3>New event{{ Request::path()}}</h3>
@if (count($errors) > 0)
    <div class="alert alert-danger">
      <ul>
          @foreach ($errors->all() as $error)
              <li>{{ $error }}</li>
          @endforeach
      </ul>
    </div>
@endif
@if(session()->has('errorMsg'))
    <div class="alert alert-danger">
        {{ session()->get('errorMsg') }}
    </div>
@endif
<form method="POST">
  {{ csrf_field() }}
  <div class="form-group">
    <label>Subject</label>
    <input type="text" class="form-control" name="eventSubject"  value="{{ old('eventSubject') }}" />
  </div>
  <div class="form-group">
    <label>Attendees (emails seperated by commas)</label>
    <input type="text" class="form-control" name="eventAttendees"  value="{{ old('eventAttendees') }}" />
  </div>
  <div class="form-row">
    <div class="col">
      <div class="form-group">
        <label>Start</label>
        <input type="datetime-local" class="form-control" name="eventStart" id="eventStart"  value="{{ old('eventStart') }}"/>
      </div>
    </div>
    <div class="col">
      <div class="form-group">
        <label>End</label>
        <input type="datetime-local" class="form-control" name="eventEnd"  value="{{ old('eventEnd') }}" />
      </div>
    </div>
  </div>
  <div class="form-group">
    <label>Body</label>
    <textarea type="text" class="form-control" name="eventBody" rows="3">{{ old('eventBody') }}</textarea>
  </div>
  <input type="submit" class="btn btn-primary mr-2" value="Create" />
  <a class="btn btn-secondary" href={{ action('CalendarController@calendar') }}>Cancel</a>
</form>
@endsection