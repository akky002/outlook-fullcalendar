@extends('layout')
<div style="padding-top: 100px;"></div>
@section('content')
<div class="jumbotron  text-center">
  <h1>PHP Graph Demo</h1>
  <p class="lead">This sample demo shows how to use the Microsoft Graph API to access a user's data like Outlook calendar groups, calendars, events from PHP.</p>
  @if(isset($userName))
    <h3>Hi {{ $userName }} Welcome !</h3>
    <p>Please use the navigation bar at the top of the page to get started.</p>
  @else
    <a href="/signin" class="btn btn-primary btn-large">Click here to sign in</a>
  @endif
</div>
@endsection