<!DOCTYPE html>
<html lang='en'>
  <head>
    <meta charset='utf-8' />
   <!-- Bootstrap -->
    <link rel="stylesheet" type='text/css' href="{{asset('assets/bootstrap3/bootstrap.min.css')}}">
    <script src="{{asset('assets/jquery.min.js')}}"></script>
    <script src="{{asset('assets/bootstrap3/bootstrap.min.js')}}"></script>
    <!-- mementJs file -->
    <script type="text/javascript" src="{{asset('assets/moment.js')}}"></script>
    <!-- fullcalneder library files -->
    <link rel="stylesheet" type='text/css' href="{{asset('assets/fullcalendar/fullcalendar.css')}}">
    <script src="{{asset('assets/fullcalendar/fullcalendar.js')}}"></script>
    <!-- Timepicker -->
    <link href="{{asset('assets/timepicker/mdtimepicker.css')}}" rel="stylesheet" type="text/css">
     <script src="{{asset('assets/timepicker/mdtimepicker.js')}}"></script>
    <!-- Datepicker -->
    <link href="{{asset('assets/datepicker/bootstrap-datepicker.min.css')}}" rel="stylesheet" type="text/css">
    <script src="{{asset('assets/datepicker/bootstrap-datepicker.min.js')}}"></script>
    <script>
      document.addEventListener('DOMContentLoaded', function() {

        //initialise datepicker
        $('.datepicker').datepicker({
          startDate: new Date(),
          format: 'yyyy-mm-dd'
        }); 
        
        //initialise mdtimepicker
        $('.timepicker').mdtimepicker({
            timeFormat: 'hh:mm:ss.000', // format of the time value (data-time attribute)
            // format: 'h:mm:ss tt',          // format of the input value
            format: 'hh:mm',          // format of the input value
            theme: 'blue',              // theme of the timepicker
            hourPadding: false,         // determines if display value has zero padding for hour value less than 10 (i.e. 05:30 PM); 24-hour format has padding by default
            clearBtn: false,            // determines if clear button is visible
            is24hour: true    
        });

        //this function is independant of fullcalender code . it is used for from validation & storaing data in localstorage
        $( "#submit" ).click(function() {
          var start_date = $('#start_date').val();var end_date = $('#end_date').val();
          var start_time = $('#start_time').val();var end_time = $('#end_time').val();
          var event_title = $('#event_title').val();
          var eventAttendees = $('#eventAttendees').val();

          var errDateFlag = errTimeFlag = errTimeReqFlag = startDateFlag = endDateFlag = titleFlag = startTimeFlag = endTimeFlag = pastStartTimeFlag = false;

          var todaysDate = moment().format("YYYY-MM-DD");var currentTime = moment().format("HH:mm:00");

          if(start_date ==''){
              startDateFlag = true;
              $('.errstartdate').show();
          }else{
              startDateFlag = false;
              $('.errstartdate').hide();
          }

          if(end_date ==''){
              endDateFlag = true;
              $('.errenddate').show();
          }else{
              endDateFlag = false;
              $('.errenddate').hide();
          }

          if(start_time ==''){
              startTimeFlag = true;
              $('.errstarttimereq').show();
          }else{
              startTimeFlag = false;
              $('.errstarttimereq').hide();
          }

          if(end_time ==''){
              endTimeFlag = true;
              $('.errendttimereq').show();
          }else{
              endTimeFlag = false;
              $('.errendttimereq').hide();
          }

          if(event_title ==''){
              titleFlag = true;
              $('.errtitle').show();
          }else{
              titleFlag = false;
              $('.errtitle').hide();
          }

          //dont allow past time to set for start time if todays date same as start date
          if(start_date == todaysDate && (start_time < currentTime) && start_time != ''){
            pastStartTimeFlag = true;
             $('.errpaststarttime').show();
          }else{
            pastStartTimeFlag = false;
            $('.errpaststarttime').hide();
          }

          //show err if start date is greater than end date
          if(moment(start_date).isAfter(moment(end_date))){
              errDateFlag = true;
              $('.errdate').show();
          }
          else{
              errDateFlag = false;
              $('.errdate').hide();
          }

          //show err if start time is greater than end time when both i.e start & end dates are same
          if(moment(start_date).isSame(end_date) && moment(start_time,"hh:mm:ss").isAfter(moment(end_time,"hh:mm:ss"))){
              errTimeFlag = true;
              $('.errtime').show();
          }
          else{
              errTimeFlag = false;
              $('.errtime').hide();
          }

          //check all validations & if all okay then allow to set events data in outlookcalendar
          if(!startDateFlag && !endDateFlag && !startTimeFlag && !endTimeFlag  && !titleFlag  && !pastStartTimeFlag  && !errDateFlag  && !errTimeFlag  && !errTimeReqFlag  && start_date != ''  && end_date != ''  && event_title != ''){
          
              var start_date_time = '';
              var end_date_time = '';
              start_date_time = start_date+'T'+start_time;
              end_date_time = end_date+'T'+end_time;

              var xmlhttp = new XMLHttpRequest();
              xmlhttp.open("POST", "createevent", true);
              xmlhttp.setRequestHeader("Content-Type", "application/json");
              xmlhttp.onreadystatechange = function() {
                if (this.readyState == 4 && this.status == 200) {
                  var myObj = JSON.parse(this.responseText);
                  document.getElementById("message").innerHTML = myObj.message;

                  if(myObj.status == '200'){
                    document.getElementById("message").style.color = "green";
                  }else{
                    document.getElementById("message").style.color = "red";
                  }
                }
              }
              var data = {'_token':'{{ csrf_token() }}','eventStart': start_date_time,'eventEnd':end_date_time,'eventSubject':event_title,'eventAttendees':eventAttendees};
              xmlhttp.send(JSON.stringify(data));
              $('#myAvaibility').modal('hide'); //uncomment if you removed below reload comment
              init();
              $(this).closest('form').find("input[type=text], textarea").val("");
          }
        });

        //fullcalender code started
        function init(){
         
          var calendarEl = document.getElementById('calendar');
          var calendar = new FullCalendar.Calendar(calendarEl, {
            // dayMinWidth: 200,
            //set header buttons
            headerToolbar: {
              left: 'prev,next today,myCustomButton',
              center: 'title',
              right: 'dayGridMonth,timeGridWeek,listWeek,timeGridDay,timeGridFourDay'
            },
            initialDate: moment().format("YYYY-MM-DD"), //to show current date
            initialView: 'dayGridMonth',
            customButtons: {
              myCustomButton: {
                text: 'custom btn',
                click: function() {
                  alert('clicked the custom button!');
                }
              }
            },
            views: {
              timeGridFourDay: {
                type: 'timeGrid',
                duration: { days: 4 },
                buttonText: '4 days'
              }
            },
            allDaySlot : false,
            navLinks: true, // can click day/week names to navigate views
            editable: false, //dissble dragging events
            // dayMaxEvents: true, // allow "more" link when too many events
            aspectRatio: 1.8, //Sets the width-to-height aspect ratio of the calendar
            scrollTime: '00:00',
            events: {
              url: 'getdefaultevents',
              method: 'POST',
              extraParams: {
                _token: "{{ csrf_token() }}",
              },
              failure: function() {
                document.getElementById('script-warning').style.display = 'block';
              }
            },
            eventClick: function(info) {
             
             //call api to delete event from outlook calendar
              if (confirm('Are you sure you want to delete ' + info.event.title + '?')) {
                var xhttp = new XMLHttpRequest();
                xhttp.onreadystatechange = function() {
                  if (this.readyState == 4 && this.status == 200) {
                    var myObj = JSON.parse(this.responseText);
                    document.getElementById("message").innerHTML = myObj.message;
                    document.getElementById("message").style.color = "green";
                  }
                };
                xhttp.open("GET", "removeevent/"+info.event.id, true);
                xhttp.send();
                info.event.remove(); //to remove event from calender
              }
            }
          });
          calendar.render(); //to render calender if it is already rendered it will rerender 
        }
        init(); 
      });
    </script>
  </head>
  <body>
    <nav class="navbar navbar-inverse">
      <div class="container-fluid">
        <div class="navbar-header">
          <a class="navbar-brand" href="/">PHP Graph Demo</a>
        </div>
        <ul class="nav navbar-nav">
          <li>
            <a href="/" class="nav-link {{$_SERVER['REQUEST_URI'] == '/' ? ' active' : ''}}">Home</a>
          </li>
          <li>
            <a href="/calendar" class="nav-link{{$_SERVER['REQUEST_URI'] == '/calendar' ? ' active' : ''}}">Events</a>
          </li>
          <li>
            <a href={{action('CalendarController@groupView')}} class="nav-link{{$_SERVER['REQUEST_URI'] == '/calendar/groups' ? ' active' : ''}}">Groups</a>
          </li>
          <li>
            <a href={{action('CalendarController@allCalendarView')}} class="nav-link{{$_SERVER['REQUEST_URI'] == '/calendar/allcalendars' ? ' active' : ''}}">Calendars</a>
          </li>
          <li class="nav-link{{$_SERVER['REQUEST_URI'] == '/fullcalendar/calendar' ? ' active' : ''}}">
            <a href={{action('FullCalendarController@calendar')}}>FullCalendar</a>
          </li>
          </ul>
      </div>
    </nav>
  <div class="container">
    <div class="jumbotron">
      <h1 class="text-center">Full Calender Events</h1>
      <!-- <h6 class="text-center">This demo will show outloook events in fullcalendar</h6> -->
      <h5 class="text-center">This demo will let you create/show outlook events from/to fullcalendar plugin.</h5>

    </div>
      <h4 id="message" class="text-center"></h4>
      <div class="row">
          <div class="col-sm-10">
              <div id='calendar'></div>
          </div>
          <div class="col-sm-2">
              <button type="button" class="btn btn-primary btn-block" data-toggle="modal" data-target="#myAvaibility">Add Event</button>
              <hr/>
                <b class="text-danger"><span>Note: To delete event please click on event.</span></b><hr/>
          </div>
      </div>
  </div>

 <!-- start avaibility Modal -->
  <div class="modal fade" id="myAvaibility" role="dialog" data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog">
      <div class="modal-content">
        <div class="modal-header">
          <button type="button" class="close" data-dismiss="modal">&times;</button>
          <h3 class="modal-title text-center text-primary">Create Event</h3>
        </div>
        <form autocomplete="off" method="post">
          <div class="modal-body">
            <div class="row">
              <div class="form-group col-sm-12">
                <label for="email">Event Name</label><span style="color: red">*</span>
                <input type="text" class="form-control" id="event_title" required="">
              </div>
              <div class="form-group col-sm-6">
                <label for="email">Start Date</label><span style="color: red">*</span>
                <input type="text" class="form-control datepicker" id="start_date" required="">
              </div>
              <div class="form-group col-sm-6">
                <label for="pwd">Start End</label><span style="color: red">*</span>
                <input type="text" class="form-control datepicker" id="end_date" required="">
              </div>
              <div class="form-group col-sm-6">
                <label for="email">Start Time:</label><span style="color: red">*</span>
                <input type="text" class="form-control timepicker" data-theme="dark" id="start_time">
              </div>
              <div class="form-group col-sm-6">
                <label for="pwd">End Time:</label><span style="color: red">*</span>
                <input type="text" class="form-control timepicker" data-theme="dark" id="end_time" />
              </div>
              <div class="form-group col-sm-12">
                <label>Attendees (emails seperated by commas)</label>
                <textarea type="text" class="form-control" name="eventAttendees" id="eventAttendees" rows="2"></textarea>
              </div>
              <div class="form-group col-sm-12">
                 <p class="text-danger text-center errtitle" style="display: none">Please enter event name.</p>
                  <p class="text-danger text-center errstartdate" style="display: none">Please select start date.</p>
                  <p class="text-danger text-center errenddate" style="display: none">Please select end date.</p>
                  <p class="text-danger text-center errdate" style="display: none">Please select valid date.</p>
                  <p class="text-danger text-center errstarttimereq" style="display: none">Please select start time.</p>
                  <p class="text-danger text-center errendttimereq" style="display: none">Please select end time.</p>
                  <p class="text-danger text-center errtime" style="display: none">Please select valid time.</p>
                  <p class="text-danger text-center errpaststarttime" style="display: none">You can't select past time as start time.</p>
              </div>
            </div>
          </div>
          <div class="modal-footer">
              <button id="submit" type="button" class="btn btn-primary" style="min-width: 110px;" >Create Event</button>
              <button  class="btn btn-default" data-dismiss="modal" style="min-width: 110px;" >Close</button>
          </div>
        </form>
      </div>
    </div>
  </div>
  </body>
</html>