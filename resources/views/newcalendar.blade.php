@extends('layout')
@section('content')
<h1>New Calendar</h1>
<form method="POST">
  {{ csrf_field() }}
	<div class="form-group">
	  <label for="sel1">Select Group:</label>
	  <select class="form-control" name="groupId" required="">
	    <option value="">Select Group</option>
	    @if(isset($groups))
	      @foreach($groups as $group)
	        <option value="{{ $group->getProperties()['id'] }}">{{ $group->getProperties()['name'] }}</option>
	      @endforeach
	    @endif
	  </select>
	</div>
	<div class="form-group">
		<label>Calendar name:</label>
		<input type="text" class="form-control" name="CalendarName" required="" />
	</div>
	<input type="submit" class="btn btn-primary mr-2" value="Create" />
	<a class="btn btn-secondary" href="{{ url()->previous() }}">Cancel</a>
</form>
@endsection