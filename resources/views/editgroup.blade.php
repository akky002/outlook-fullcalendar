@extends('layout')
@section('content')
<h1>Edit Group</h1>
@if (count($errors) > 0)
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif
<form method="POST" action="{{ action('CalendarController@updateGroup') }}">
  {{ csrf_field() }}
  <div class="form-group">
    <label>Group name</label>
    <input type="text" class="form-control" name="groupName" value="{{$group->getProperties()['name']}}" />
    <input type="hidden" class="form-control" name="groupId"  value="{{$group->getProperties()['id']}}"required="" />
	</div>
  </div>
  <input type="submit" class="btn btn-primary mr-2" value="Update" />
  <a class="btn btn-secondary" href="{{ url()->previous() }}">Cancel</a>
</form>
@endsection