@extends('layout')
@section('content')
<h1>New Group</h1>
<form method="POST">
  {{ csrf_field() }}
  <div class="form-group">
    <label>Group name</label>
    <input type="text" class="form-control" name="groupName" required=""/>
  </div>
  <input type="submit" class="btn btn-primary mr-2" value="Create" />
  <a class="btn btn-secondary" href="{{ action('CalendarController@groupView') }}" >Cancel</a>
</form>
@endsection